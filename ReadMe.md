# Generalne napomene
- Na emulatoru ili uređaju na kojem testirate obavezno isključite animacije (inače testovi neće prolaziti)


![slika](animationoff.png)

- Importe iz testova ne smijete mijenjati. Ako su pogrešni promijenite strukturu projekta tako da:
- Naziv paketa projekta mora biti
```
    package ba.etf.rma22.projekat
```
- Modeli trebaju biti u paketu:
```
    ba.etf.rma22.projekat.data.models
```
- Repozitoriji trebaju biti u paketu:
```
    ba.etf.rma22.projekat.data.repositories
```
- Početna aktivnost se treba zvati:
```
    MainActivity
```

# Dependencies

    androidTestImplementation 'androidx.test.ext:junit:1.1.3'
    androidTestImplementation 'androidx.test:core-ktx:1.4.0'
    androidTestImplementation 'androidx.test.ext:junit-ktx:1.1.3'
    testImplementation("org.hamcrest:hamcrest:2.2")
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.4.0'
    androidTestImplementation 'androidx.test.espresso:espresso-intents:3.4.0'
    androidTestImplementation 'com.android.support.test.espresso:espresso-contrib:3.4.0'
    testImplementation "io.mockk:mockk:1.12.3"
    androidTestImplementation "io.mockk:mockk-android:1.12.3"
    implementation 'com.github.wseemann:FFmpegMediaMetadataRetriever-native:1.0.15'


# Testovi S4
Dodani su testovi za spiralu 4. U repozitorije ne proslijeđujte kontekst. Kontekst možete postaviti prilikom otvaranja glavne aktivnosti (u onCreate) i onda ga koristiti interno ne mijenjajući definiciju metoda repozitorija. Testove za spiralu 4 stavite u **_androidTest_** folder a ne u ~~unit test~~!