@file:Suppress("PackageDirectoryMismatch")
package ba.etf.rma22.projekat

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.UtilTestClass.Companion.hasItemCount
import ba.etf.rma22.projekat.UtilTestClass.Companion.itemTest
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.internal.MethodSorter
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4::class)
class UpisTest {

    @get:Rule
    val intentsTestRule = ActivityScenarioRule<MainActivity>(MainActivity::class.java)
    @Test
    fun A_upisTest() {

        Espresso.onView(ViewMatchers.withId(R.id.filterAnketa)).perform(ViewActions.click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`("Sve moje ankete")
            )
        ).perform(ViewActions.click())
        val anketePrije = AnketaRepository.getMyAnkete()
        val anketePrijeSize = anketePrije.size
        Espresso.onView(ViewMatchers.withId(R.id.listaAnketa))
            .check(hasItemCount(anketePrije.size))
        for (anketa in anketePrije) {
            itemTest(R.id.listaAnketa, anketa)
        }
        Espresso.onView(ViewMatchers.withId(R.id.upisDugme)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.odabirGodina)).perform(ViewActions.click())
        val nedodjeljeneAnkete = AnketaRepository.getAll().minus(AnketaRepository.getMyAnkete())
        val nedodjeljenaIstrazivanja = IstrazivanjeRepository.getAll().minus(IstrazivanjeRepository.getUpisani())

        var grupaVrijednost = ""
        var nazivIstrazivanja = ""
        var godinaVrijednost = -1
        for (nk in nedodjeljeneAnkete) {
            for (np in nedodjeljenaIstrazivanja) {
                if (nk.nazivIstrazivanja == np.naziv) {
                    grupaVrijednost = nk.nazivGrupe
                    godinaVrijednost = np.godina
                    nazivIstrazivanja = np.naziv

                }
            }
        }
        ViewMatchers.assertThat(
            "Nema neupisanih istrazivanja sa anketama",
            godinaVrijednost,
            CoreMatchers.not(CoreMatchers.`is`(-1))
        )

        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(godinaVrijednost.toString())
            )
        ).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.odabirIstrazivanja)).perform(ViewActions.click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(nazivIstrazivanja)
            )
        ).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.odabirGrupa)).perform(ViewActions.click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(grupaVrijednost)
            )
        ).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.dodajIstrazivanjeDugme)).perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.filterAnketa)).perform(ViewActions.click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`("Sve moje ankete")
            )
        ).perform(ViewActions.click())
        val anketePoslije = AnketaRepository.getMyAnkete()

        Espresso.onView(ViewMatchers.withId(R.id.listaAnketa))
            .check(hasItemCount(anketePoslije.size))
        for (anketa in anketePoslije) {
            itemTest(R.id.listaAnketa, anketa)
        }

        ViewMatchers.assertThat(
            "Nije dodana anketa nakon upisanog istrazivanja ",
            anketePrijeSize,
            CoreMatchers.`is`(Matchers.lessThan(anketePoslije.size))
        )

    }

    @Test
    fun B_upisTest(){
        A_upisTest()
    }
}