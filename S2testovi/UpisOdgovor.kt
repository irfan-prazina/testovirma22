package ba.etf.rma22.projekat

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers
import org.hamcrest.core.AllOf.allOf
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class UpisOdgovor {
    @get:Rule
    val intentsTestRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun A_ucitajuSePitanjaIOdgovori() {
        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToFirst())
        onView(withId(R.id.filterAnketa)).perform(click())
        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Sve moje ankete"))).perform(click())
        val ankete = AnketaRepository.getMyAnkete()
        onView(withId(R.id.listaAnketa)).perform(
            RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(
                ViewMatchers.hasDescendant(ViewMatchers.withText(ankete[0].naziv)),
                ViewMatchers.hasDescendant(ViewMatchers.withText(ankete[0].nazivIstrazivanja))
            ), click()))
        val pitanja = PitanjeAnketaRepository.getPitanja(ankete[0].naziv, ankete[0].nazivIstrazivanja)
        for ((indeks,pitanje) in pitanja.withIndex()) {
            onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(indeks))
            onView(Matchers.allOf(isDisplayed(), withId(R.id.tekstPitanja))).check(matches(
                ViewMatchers.withText(pitanja[indeks].tekst)
            ))
            for(odgovor in pitanje.opcije){
                onData(CoreMatchers.`is`(odgovor)).inAdapterView(allOf(withId(R.id.odgovoriLista),
                    isDisplayed())).check(
                    matches(isDisplayed()))
            }
        }
    }

    @Test
    fun B_upisValidan(){
        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(1))
        var nedodijeljeneAnkete = AnketaRepository.getAll().minus(AnketaRepository.getMyAnkete()).minus(AnketaRepository.getFuture()).minus(AnketaRepository.getDone()).minus(AnketaRepository.getNotTaken())
        val nedodijeljenaIstrazivanja = IstrazivanjeRepository.getAll().minus(IstrazivanjeRepository.getUpisani())
        var grupaVrijednost = ""
        var istrazivanjeNaziv = ""
        var godinaVrijednost = -1

        vanjska@ for (nk in nedodijeljeneAnkete) {
            var istrazivanjeAnkete:Istrazivanje? = nedodijeljenaIstrazivanja.find { istrazivanje -> istrazivanje.naziv==nk.nazivIstrazivanja }
            if (istrazivanjeAnkete!=null) {
                grupaVrijednost = nk.nazivGrupe
                godinaVrijednost = istrazivanjeAnkete.godina
                istrazivanjeNaziv = istrazivanjeAnkete.naziv
                break@vanjska
            }
        }
        ViewMatchers.assertThat(
            "Nema neupisanih istraživanja sa anketama",
            godinaVrijednost,
            CoreMatchers.not(CoreMatchers.`is`(-1))
        )
        onView(withId(R.id.odabirGodina)).perform(click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(godinaVrijednost.toString())
            )
        ).perform(click())
        onView(withId(R.id.odabirIstrazivanja)).perform(click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(istrazivanjeNaziv)
            )
        ).perform(click())
        onView(withId(R.id.odabirGrupa)).perform(click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(grupaVrijednost)
            )
        ).perform(click())
        onView(withId(R.id.dodajIstrazivanjeDugme)).perform(click())
        onView(ViewMatchers.withSubstring("Uspješno ste upisani u grupu"))
    }

    @Test
    fun C_anketeAtPosition0(){
        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(0))
        onView(withId(R.id.filterAnketa)).check(matches(isDisplayed()))
    }

    @Test
    fun D_istrazivanjaUpisAtPosition1(){
        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(1))
        onView(withId(R.id.odabirGodina)).check(matches(isDisplayed()))
    }


}